$( document ).ready(function() {

    // увеличение и уменьшение товаров в инпуте

    // формат цифр
    function number_format( str ){
        return str.replace(/(\s)+/g, '').replace(/(\d{1,3})(?=(?:\d{3})+$)/g, '$1 ');
    };

    // +/- в полях
    $(document).on('click', '.js--counter--button', function(){
        var block = $(this);
        var current_value = $(this).parents('.counter').find('.inp').prop('value');
        var step = 1;
        if($(this).hasClass('js-remove') == true && (current_value == 1  || current_value == undefined)){
            return false
        }else{
            current_value = current_value.replace(/\s+/g, '');
            current_value = Number(current_value);
            ($(this).hasClass('js-remove') == true) ? (current_value = current_value - step) : (current_value = current_value + step);
            $(this).parents('.counter').find('.inp').prop('value', current_value);
        }
        return false;
    });

    // ввод только цифр в поле количетво
    $(document).on('keydown', '.input-number', function(e){input_number();});

    // ввод количества с клавиатуры
    $(document).on('input','.input-number', function(){

        if($(this).data("lastval")!= $(this).val()) {
            if($(this).val() == ''){
                $(this).prop('value',1)
            }
            else{
                var value = $(this).prop('value');
                if(value[value.length - 1] =='.' && value.length == 1){
                    $(this).prop('value','0.')
                }
                else{
                    if(value[value.length - 1] !='.'){
                        value = value.replace(/\s+/g, '');
                        value = Number(value);
                        value = value.toString();
                        value = number_format(value);
                        if(value == "NaN"){
                            $(this).prop('value',1)
                        }else{
                            $(this).prop('value',value);
                        }
                    }
                    else{
                        if(value[value.length - 1] =='.' && value[value.length - 2] =='.'){
                            value = value.slice(0,-1)
                            $(this).prop('value',value)
                        }
                    }
                }

            };
            $(this).data("lastval", $(this).val());

        };
    });

    // ввод только цифр в поле
    var input_number = function(){
        var allow_meta_keys=[86, 67, 65];
        if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9  || event.keyCode == 27 || event.keyCode == 110 || event.keyCode == 191 ||
            // Разрешаем: Ctrl+A
            ($.inArray(event.keyCode,allow_meta_keys) > -1 && (event.ctrlKey === true ||  event.metaKey === true)) ||
            // Разрешаем: home, end, влево, вправо
            (event.keyCode >= 35 && event.keyCode <= 39)) {
            // Ничего не делаем
            return;
        }
        else {
            // Обеждаемся, что это цифра, и останавливаем событие keypress
            if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                event.preventDefault();
            }
        }
    };

    // БОКОВАЯ ПАНЕЛЬ

    new mlPushMenu( document.getElementById( 'mp-menu' ), document.getElementById( 'trigger' ) );

    // КАРТА
     if ($('#map').length) {
        function initMap() {
            var lat = Number($('.tabs__caption li.active').attr('data-lat'));
            var lng = Number($('.tabs__caption li.active').attr('data-lng'));
            var title = $('.tabs__caption li.active').attr('data-title');
            var adress = $('.tabs__caption li.active').attr('data-adress');
            var phone = $('.tabs__caption li.active').attr('data-phone');
            var phone_cal = $('.tabs__caption li.active').attr('data-phone-number');
            var mail = $('.tabs__caption li.active').attr('data-mail');
          var map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: -34.397, lng: 150.644},
            zoom: 13
          });
          var infoWindow = new google.maps.InfoWindow({map: map});

          var pos = {
            lat: lat,
            lng: lng
          };

          infoWindow.setPosition(pos);
          infoWindow.setContent('<div class="map-popup">' +
              '<span class="map-popup--title">' + title + '</span>' +
              '<span class="map-popup--adress">' + adress + '</span>' +
              '<a href="tel:' + phone_cal + '" class="map-popup--adress">Tel:' + phone + '</span>' +
              '<a href="mailto:' + mail + '" class="map-popup--adress">Email:' + mail + '</span>' +
          '</div>');
          map.setCenter(pos);

        }
        initMap();
    }


    $('ul.tabs__caption').on('click', 'li:not(.active)', function() {
        $(this)
          .addClass('active').siblings().removeClass('active')
          .closest('div.tabs').find('div.tabs__content').removeClass('active').eq($(this).index()).addClass('active');
        if ($('#map').length) {
            initMap();
        }
      });

    if ($('.index-slider').length) {
        $('.index-slider').bxSlider({
            infiniteLoop: true,
            adaptiveHeight: true,
            smartSpeed: 500,
            slideMargin: 0,
            auto: true,
            mode: 'fade',
            touchEnabled: true
        });
    }

    if ($('.select-styled').length) {
        $('.select-styled').each(function(){
            $(this).styler();
        });
    };

    if ($('.house-slider').length) {
        $('.house-slider').owlCarousel({
            loop: false,
            nav: true,
            items: 3,
            smartSpeed: 500,
            fluidSpeed: 500,
            margin: 0,
            navText: [,],
            autoplay: false,
            responsive : {
                0 : {
                    items: 1
                },
                500 : {
                    items: 1
                },
                600 : {
                    items: 2
                },
                768 : {
                    items: 2
                },
                1000 : {
                    items: 3
                }
            }
        });
    }

    $(".zoom-img").imagezoomsl({
         zoomrange: [3, 3],
         magnifiersize: [530, 340]
    });

    $(".zoom-mobile").imagezoomsl({
        zoomrange: [1, 12],
        zoomstart: 4,
        innerzoom: true,
        magnifierborder: "none"
    });

    $('.aside-nav .has-sub').hover(function(){
        var block = $(this),
            top_el = $(this).offset().top,
            top_page = $(document).scrollTop(),
            page_height =$(window).height(),
            counter = $(this).find('.sub-aside-nav li').length,
            ul_height = Math.round((page_height * 0.4)/28) * 28;

            block.addClass('open');

            if((top_el - top_page)>= page_height/2){
                block.addClass('bottom_ul')
            }

            if(counter > Math.round((page_height * 0.4)/28)){
                block.find('.sub-aside-nav').css({'height': ul_height});
                block.addClass('center_ul')
            }

    }, function(){
        var block = $(this);
        block.removeClass('open');
        block.removeClass('bottom_ul');
    });

    $('.tablet-nav .has-sub ').click(function(){
        $(this).next('.sub-aside-nav').find('ul').stop().slideToggle(200);
    });


})